import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.relation.Tree
import syntax.FWJavaParserSyntax.{ASTNode, Program}


class Types(tree : Tree[ASTNode, Program]) extends Attribution {

    import syntax.FWJavaParserSyntax._
    import org.bitbucket.inkytonik.kiama.util.Messaging.{collectMessages, error, info, Messages, noMessages}
    import syntax.FWJavaParserPrettyPrinter.show
    import tree._

    type Defs = Vector[FPDecl]
    
    // Pretty-printing helpers
    
    def ppe(e : Expr) : String = {
        val s = show(e)
        if (s.length < 10)
            s
        else
            s"${s.substring(0, 10)} ..."
    }

    def ppt(ot : Option[String]) : String =
        ot match {
            case Some(c) =>
                c
            case _ =>
                "unknown"
        }

    def ppn(names : Vector[String]) : String =
        names.mkString(", ")

    def ppd(defs : Defs) : String =
        defs.map(show(_)).mkString(", ")
        
    // Accessors

    def className(l : ClassDecl) : String =
        l.identifier

    def superClassName(l : ClassDecl) : String =
        l.typeUse.identifier

    def fieldNames(l : ClassDecl) : Vector[String] =
        l.optFPDecls.map(_.idnDef.identifier)

    def fieldTypes(l : ClassDecl) : Vector[String] =
        l.optFPDecls.map(_.typeUse.identifier)
        
    def methodName(m : MethodDecl) : String =
        m.identifier
        
    def argTypes(m : MethodDecl) : Vector[String] =
        m.optFPDecls.map(_.typeUse.identifier)
        
    def returnType(m : MethodDecl) : String =
        m.typeUse.identifier
        
    def constructorName(k : CtorDecl) : String =
        k.typeUse.identifier      
        
    def constructorParams(k : CtorDecl) : Defs =
        k.optFPDecls
        
    def superArgs(k : CtorDecl) : Vector[VarUse] =
        k.varUses.optVarUses
        
    def superArgNames(k : CtorDecl) : Vector[String] =
        superArgs(k).map(_.identifier)
        
    // Top-level message collection
        
    lazy val messages : Messages =
        collectMessages(tree) {
            case d @ parent.pair(IdnDef(i), parent(p)) =>
                info(d, s"$i def: ${ppt(findType(env(d), i))}")
            case e : Expr =>
                info(e, s"${ppe(e)}: ${ppt(tipeName(e))}")
            case l : ClassDecl =>
                classOK(l)
        }
        
    // OK checking    
    
    val classOK : ClassDecl => Messages =
        attr {
            case l =>
                val cycles = superClassCycle(l)(l)
                if (cycles.isEmpty)
                    constructorOK(l) ++ l.optMethodDecls.flatMap(methodOK)
                else 
                    cycles
        }
        
    val constructorOK : ClassDecl => Messages =
        attr {
            case l =>
                superClassDefined(l) ++
                    constructorNameOK(l) ++ constructorParamsOK(l) ++
                    initialisersOK(l) ++ superConstructorCallOK(l)
        }
        
    val superClassDefined : ClassDecl => Messages =
        attr {
            case l =>
                if ((superClassName(l) == "Object") || decl(superClassName(l))(l).isDefined)
                    noMessages
                else 
                    error(l.typeUse, s"superclass ${superClassName(l)} is not defined")
        }
        
    val superClassCycle : ClassDecl => ClassDecl => Messages =
        paramAttr {
            case ClassDecl("Object", _, _, _, _) => {
                case _ => noMessages
            }
            case base => {
                case ClassDecl("Object", _, _, _, _) => 
                    noMessages
                case l =>
                    decl(superClassName(l))(l) match {
                        case Some(l2) =>
                            if (className(l2) == className(base))
                                error(base, s"superclass cycle from ${className(base)}") 
                            else 
                                superClassCycle(base)(l2)
                        case None =>
                            noMessages
                    }
            }
        }
        
    val constructorNameOK : ClassDecl => Messages =
        attr {
            case l =>
                if (className(l) == constructorName(l.ctorDecl))  
                    noMessages
                else
                    error(l.ctorDecl, s"constructor name ${constructorName(l.ctorDecl)} is not class name ${className(l)}")
        }
        
    val constructorParamsOK : ClassDecl => Messages =
        attr {
            case l =>
                fieldsRef(l) match {
                    case Some(defs) =>
                        if (constructorParams(l.ctorDecl) == defs)
                            noMessages
                        else 
                            error(l.ctorDecl, s"constructor args (${ppd(constructorParams(l.ctorDecl))}) should be the fields (${ppd(defs)})")
                    case None =>
                        noMessages
                }
        }
        
    val initialisersOK : ClassDecl => Messages = 
        attr {
            case l =>
                l.ctorDecl.fieldInits.optFieldInits.flatMap(initialiserOK)
        }
        
    val initialiserOK : FieldInit => Messages = 
        attr {
            case parent.pair(i : FieldInit, parent(parent(l : ClassDecl))) =>
                val f = fieldNames(l)(index(i))
                if ((i.varUse1.identifier == f) && (i.varUse2.identifier == f))
                    noMessages 
                else
                    error(i, s"initialiser must be this.$f = $f")
        }
        
    val superConstructorCallOK : ClassDecl => Messages = 
        attr {
            case l =>
                decl(superClassName(l))(l) match {
                    case Some(l2) =>
                        superArgs(l.ctorDecl).map(superConstructorArgNameOK(l2)).flatten ++
                            superArgs(l.ctorDecl).map(superConstructorArgTypeOK(l2)).flatten
                    case _ =>
                        noMessages
                }
        }
        
    val superConstructorArgNameOK : ClassDecl => VarUse => Messages =
        paramAttr {
            case l => {
                case varUse @ VarUse(x) =>
                    val n = index(varUse)
                    val f = fieldNames(l)(n)
                    if (x == f)
                        noMessages 
                    else
                        error(varUse, s"super arg $n must be $f, not $x")
            }
        }

    val superConstructorArgTypeOK : ClassDecl => VarUse => Messages =
        paramAttr {
            case l => {
                case varUse =>
                    val n = index(varUse)
                    val t1 = fieldTypes(l)(n)
                    varUseTypeRef(varUse) match {
                        case Some(ClassDecl(t2, _, _, _, _)) if t1 != t2 =>
                            error(varUse, s"super arg $n type must be $t1, not $t2")
                        case _ =>
                        noMessages 
                    }
            }
        }
        
    val methodOK : MethodDecl => Messages =
        attr {
            case m =>
                returnTypeOK(m) ++ overrideOK(m)
        }
        
    val returnTypeOK : MethodDecl => Messages =
        attr {
            case m =>
                if (optsubtype((bodyType(m), returnType(m)))(m))
                    noMessages 
                else
                    error(m, s"body type ${bodyType(m)} is not a subtype of declared return type ${returnType(m)}")
        }

    val bodyType : MethodDecl => Option[ClassDecl] =
        attr {
            case m =>
                tipeRef(m.expr)
        }
        
    val overrideOK : MethodDecl => Messages =
        attr {
            case parent.pair(m : MethodDecl, l : ClassDecl) =>
                decl(superClassName(l))(l) match {
                    case Some(l2) =>
                        method(methodName(m))(l2) match {
                            case Some(sm) =>
                                if ((returnType(m) != returnType(sm)) || (argTypes(m) != argTypes(sm)) )
                                    error(m, s"""method type (${ppn(argTypes(m))}) : ${returnType(m)} does not match overridden type (${ppn(argTypes(sm))}) : ${returnType(sm)}""")
                                else
                                    noMessages                             
                            case None =>
                                noMessages
                        }
                    case None =>
                        noMessages
                }
        }
        
    // Names via environments
        
    val env : ASTNode => Defs =
        attr {
            case l @ ClassDecl(c, _, _, _, _) =>
            FPDecl(TypeUse(c), IdnDef("this")) +: fieldsRef(l).get

            case k : CtorDecl =>
                k.optFPDecls

            case parent.pair(m : MethodDecl, p) =>
                m.optFPDecls ++ env(p)
            
            case parent(p) =>
                env(p)

            case _ =>
                Vector()
        }
        
    // Types

    def findType(defs : Defs, x : String) : Option[String] =
        defs.collectFirst {
            case FPDecl(TypeUse(c), IdnDef(y)) if x == y =>
                c
        }

    // def findTypeName(defs : Defs, x : String) : Option[String] = 
    //     defs.collectFirst {
    //         case n@FPDecl(TypeUse(c), IdnDef(y)) if x == y =>
    //             decl(c)(n) match {
    //                 case Some(ClassDecl(i, _, _, _, _)) => Some(i)
    //                 case None => None
    //             }
    //     }.getOrElse(None)
    
    def findFP(defs : Defs, x : String) : Option[FPDecl] =
        defs.collectFirst {
            case fp @ FPDecl(_, IdnDef(y)) if x == y =>
                fp
        }

        
    // val varUseType : VarUse => Option[String] = 
    //     attr {
    //         case u @ VarUse(x) =>
    //             findType(env(u), x)
    //     }


    val varUseTypeRef : VarUse => Option[ClassDecl] = 
        attr {
            case u @ VarUse(x) =>
                findFP(env(u), x) match {
                    case Some(FPDecl(TypeUse(y), _)) =>
                        decl(y)(u)
                    case None =>
                        None
                }
        }
        
    val tipeRef : Expr => Option[ClassDecl] =
        attr {
            // VAR
            case EIdn(u) =>
                varUseTypeRef(u)
                
            // FLD
            case n @ EFld(e, VarUse(f)) =>
                tipeRef(e) match {
                    case Some(c) =>
                        fieldsRef(c) match {
                            case Some(defs) =>
                                findFP(defs, f) match {
                                    case Some(FPDecl(TypeUse(y), _)) =>
                                        decl(y)(n)
                                    case None =>
                                        None
                                }
                            case _ =>
                                None
                        }
                    case None =>
                        None
                }
                
            // CALL
            case n @ ECall(e, VarUse(m), es) =>
                tipeRef(e) match {
                    case Some(c) =>
                        method(m)(c) match {
                            case Some(MethodDecl(TypeUse(r), _, defs, _)) =>
                                if (esubtypes((es, defs))(n))
                                    decl(r)(n)
                                else
                                    None
                            case None =>
                                None
                        }
                    case None =>
                        None
                }
            
            // NEW
            case n @ ENew(TypeUse(c), es) =>
                decl(c)(n) match {
                    case Some(c) =>
                        fieldsRef(c) match {
                            case Some(flds) =>
                                if (esubtypes((es, flds))(n))
                                    Some(c)
                                else
                                    None
                            case _ =>
                                None
                        }
                    case None =>
                        None
                }
                
            // UCAST, DCAST
            case n@ECast(TypeUse(c), e) =>
                tipeRef(e) match {
                    case Some(d) =>
                        decl(c)(n)
                    case None =>
                        None
                }
                
        }
        


    val tipeName : Expr => Option[String] =
        attr {
            case e =>
                tipeRef(e) match {
                    case Some(ClassDecl(id, _, _, _, _)) =>
                        Some(id)
                    case None =>
                        None
                }
            // // VAR
            // case EIdn(u) =>
            //     varUseType(u)
                
            // // FLD
            // case EFld(e, VarUse(f)) =>
            //     tipeName(e) match {
            //         case Some(c) =>
            //             fields(c)(e) match {
            //                 case Some(defs) =>
            //                     findType(defs, f)
            //                 case _ =>
            //                     None
            //             }
            //         case None =>
            //             None
            //     }
                
            // // INV
            // case n @ ECall(e, VarUse(m), es) =>
            //     tipeName(e) match {
            //         case Some(c) =>
            //             decl(c)(e) match {
            //                 case Some(l) => 
            //                     method(m)(l) match {
            //                         case Some(MethodDecl(TypeUse(r), _, defs, _)) =>
            //                             if (esubtypes((es, defs))(n))
            //                                 Some(r)
            //                             else
            //                                 None
            //                         case None =>
            //                             None
            //                     }
            //                 case None =>
            //                     None
            //             }
            //         case None =>
            //             None
            //     }
            
            // // NEW
            // case n @ ENew(TypeUse(c), es) =>
            //     fields(c)(n) match {
            //         case Some(flds) =>
            //             if (esubtypes((es, flds))(n))
            //                 Some(c)
            //             else
            //                 None
            //         case _ =>
            //             None
            //     }
                
            // // UCAST, DCAST
            // case ECast(TypeUse(c), e) =>
            //     tipeName(e) match {
            //         case Some(d) =>
            //             Some(c)
            //         case None =>
            //             None
            //     }
                
        }
        
    // ClassDeclookups
        
    val decl : String => ASTNode => Option[ClassDecl] =
        paramAttr {
            case "Object" => {
                case _ => 
                    Some(ClassDecl("Object", 
                    TypeUse("Object"), 
                    Vector(), 
                    CtorDecl(
                        TypeUse("Object"), 
                        Vector(), 
                        VarUses(Vector()), 
                        FieldInits(Vector())
                    ),
                    Vector()
                    ))
            }
            case c => {
                case parent(p) =>
                    decl(c)(p)
                case Program(classDecls, _) => {
                    classDecls.find(classDecl => className(classDecl) == c)
                }
            }
        }
        
    val method : String => ClassDecl => Option[MethodDecl] =
        paramAttr {
            case m => {
                case ClassDecl("Object", _, _, _, _) =>
                    None
                case l1 @ ClassDecl(_, _, _, _, ms) =>
                    ms.find(_.identifier == m) match {
                        case Some(meth) =>
                            Some(meth)
                        case None =>
                            superClass(l1) match {
                                case Some(l2) =>
                                    method(m)(l2)
                                case None => 
                                    None
                            }
                    }
            }
        }
        
    // val fields : String => ASTNode => Option[Defs] =
    //     paramAttr {
    //         case "Object" => {
    //             case n =>
    //                 Some(Vector())
    //         }
    //         case c => {
    //             case n =>
    //                 decl(c)(n) match {
    //                     case Some(ClassDecl(_, i @ TypeUse(sc), defs1, _, _)) =>
    //                         fields(sc)(i) match {
    //                             case Some(defs2) =>
    //                                 Some(defs1 ++ defs2)
    //                             case None =>
    //                                 Some(defs1)
    //                         }
    //                     case None =>
    //                         None
    //                 }
    //         }
    //     }

    val superClass : ClassDecl => Option[ClassDecl] = 
        attr {
            case n @ ClassDecl(_, TypeUse(sc), _, _, _) =>
                decl(sc)(n)
        }

    val fieldsRef : ClassDecl => Option[Defs] =
        attr {
            case ClassDecl("Object", _, _, _, _) =>
                Some(Vector())
            case n @ ClassDecl(_, i @ TypeUse(sc), defs1, _, _) => {
                decl(sc)(n) match {
                    case Some(sup) =>
                        fieldsRef(sup) match {
                            case Some(defs2) =>
                                Some(defs1 ++ defs2)
                            case None =>
                                Some(defs1)
                        }
                    case None =>
                        None
                }
            }
        }
        
    // Sub-typing

    val esubtypes : CachedParamAttribute[(Vector[Expr], Defs), ASTNode, Boolean] =
        paramAttr {
            case (es, defs) => {
                case n =>
                    val ts = es.map(tipeRef)
                    (ts.length == defs.length) &&
                        (!ts.contains(None)) &&
                        subtypes((ts.map(_.get), defs))(n)
            }
        }

    val subtypes: CachedParamAttribute[(Vector[ClassDecl], Defs), ASTNode, Boolean] =
        paramAttr {
            case (ts, defs) => {
                case n =>
                    ts.zip(defs).forall {
                        case (c, FPDecl(TypeUse(sn), _)) =>
                            subtypeOf(sn)(c)
                    }
            }
        }
    
    val subtypeOf : CachedParamAttribute[String, ClassDecl, Boolean] =
        paramAttr {
            case "Object" => {
                case _ => true
            }
            case pn => {
                case ClassDecl("Object", _, _, _, _) => false
                case ClassDecl(pn2, _, _, _, _) if pn2 == pn =>
                    true
                case n => {
                    superClass(n) match {
                        case Some(pc) =>
                            subtypeOf(pn)(pc)
                        case None =>
                            false
                    }
                }
            }
        }
        
    // val subtype: CachedParamAttribute[(String, String), ASTNode, Boolean] =
    //     paramAttr {
    //         case ("Object", c2) => {
    //             case _ => c2 == "Object"
    //         }
    //         case (c1, c2) => {
    //             case n =>
    //                 (c1 == c2) || 
    //                 (decl(c1)(n) match {
    //                     case Some(ClassDecl(_, TypeUse(c3), _, _, _)) =>
    //                         if (c2 == c3)
    //                             true 
    //                         else 
    //                             subtype((c3, c2))(n)
    //                     case _ =>
    //                         false
    //                 })
    //         }
    //     }
        
    val optsubtype: CachedParamAttribute[(Option[ClassDecl], String), ASTNode, Boolean] =
        paramAttr {
            case (Some(c1), c2) => {
                case n =>
                    subtypeOf(c2)(c1)
            }
            case (None, _) => {
                case n =>
                    true
            }
        }
        
}
